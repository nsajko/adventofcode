// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

auto read() -> std::vector<long> {
	std::vector<long> ret{};

	for (;;) {
		long n;
		std::cin >> n;
		ret.push_back(n);
		if (std::cin.get() != ',') {
			break;
		}
	}

	return ret;
}

auto fuel(const std::vector<long> &v, long target) -> long {
	long sum{0};

	for (long start: v) {
		long d{target - start};
		if (d < 0) {
			d = -d;
		}
		sum += d*(d + 1)/2;
	}

	return sum;
}

auto f() -> long {
	std::vector<long> v(read());

	const auto [min, max]{std::ranges::minmax_element(v)};

	long min_fuel{fuel(v, *max)};

	for (long i{*min}; i < *max; i++) {
		long t{fuel(v, i)};
		if (t < min_fuel) {
			min_fuel = t;
		}
	}

	return min_fuel;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << f() << '\n';
}
