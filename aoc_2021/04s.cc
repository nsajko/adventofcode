// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

class BingoBoard {
public:

	std::array<signed char, 5 * 5> board;

	std::array<signed char, 5 * 5> marked{};

	std::array<signed char, 5> marked_counts_rows{};
	std::array<signed char, 5> marked_counts_cols{};

	using RowCol = std::pair<signed char, signed char>;

	// Helps with finding numbers on the board.
	std::array<std::pair<short, RowCol>, 5 * 5> search_help;

	auto sum() const -> long {
		long s{0};
		for (long i{0}; i < 5 * 5; i++) {
			if (marked[i] == 0) {
				s += board[i];
			}
		}
		return s;
	}

	BingoBoard(const std::array<signed char, 5 * 5> &b): board{b} {
		// Initialize search_help.
		for (long i{0}; i < 5 * 5; i++) {
			long row{i / 5}, col{i % 5};
			short data{board[i]};
			search_help[i] = std::pair<short, RowCol>{data, {row, col}};
		}
		std::ranges::sort(search_help);
	}

private:

	auto find(short n) const {
		namespace r = std::ranges;
		return r::equal_range(search_help, n, r::less{}, &std::pair<short, RowCol>::first);
	}

public:

	auto update(short n) -> bool {
		bool done{false};
		for (std::pair<short, RowCol> pair: find(n)) {
			int row{pair.second.first}, col{pair.second.second};
			marked[5*row + col] = 1;
			marked_counts_rows[row]++;
			marked_counts_cols[col]++;
			if (marked_counts_rows[row] == 5 || marked_counts_cols[col] == 5) {
				done = true;
			}
		}
		return done;
	}
};

auto read_bingo_board() -> std::array<signed char, 5 * 5> {
	std::array<signed char, 5 * 5> ret;

	for (int i{0}; i < 5 * 5; i++) {
		short n;
		std::cin >> n;
		ret[i] = n;
	}
	std::cin.get();

	return ret;
}

struct Data {
public:

	std::vector<signed char> numbers;
	std::vector<BingoBoard> boards;

	// Reads from stdin.
	Data() {
		// Fill numbers.
		for (;;) {
			short n;
			std::cin >> n;
			numbers.push_back(n);
			if (std::cin.get() == '\n') {
				break;
			}
		}

		// Fill boards.
		for (; std::cin.get() == '\n';) {
			boards.push_back(BingoBoard{read_bingo_board()});
		}
	}
};

// Return value: first is index to boards, second is
// the number that was just called when the board won.
auto g(Data &data) -> std::pair<int, int> {
	std::vector<signed char> board_is_done(data.boards.size(), 0);
	long done_boards{0};

	int i, num;
	for (signed char n: data.numbers) {
		num = n;
		i = 0;
		for (BingoBoard &board: data.boards) {
			if ((board_is_done[i] == 0) && board.update(n)) {
				board_is_done[i] = 1;
				done_boards++;
				if (done_boards == data.boards.size()) {
					goto FIN;
				}
			}
			i++;
		}
	}

FIN:
	return {i, num};
}

auto f() -> long {
	Data data{};

	std::pair<int, int> results{g(data)};

	long sum{data.boards[results.first].sum()};
	long last{results.second};

	std::cerr << sum << ' ' << last << '\n';

	return sum * last;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << f() << '\n';
}
