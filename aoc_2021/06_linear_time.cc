// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

template<typename T, long n>
constexpr
auto greater_than_or_equal(T m) -> bool {
	return static_cast<T>(n) <= m;
}

constexpr
auto nonnegative(auto m) -> bool {
	return greater_than_or_equal<0>(m);
}

constexpr
auto positive(auto m) -> bool {
	return greater_than_or_equal<1>(m);
}

template<long reset_age, long spawn_age>
requires (0 <= reset_age && 0 <= spawn_age)
class Shoal {
	static constexpr long max_age{std::max(reset_age, spawn_age)};
	std::array<long, max_age + 1> age_to_count;

public:

	Shoal(const std::array<long, max_age + 1> &age_to_count): age_to_count{age_to_count} { }

	auto sum() const -> long {
		return std::reduce(age_to_count.cbegin(), age_to_count.cend());
	}

	auto advance() -> void {
		long preg{age_to_count[0]};

		// Shift.
		for (long i{0}; i < max_age; i++) {
			age_to_count[i] = age_to_count[i + 1];
		}
		age_to_count[max_age] = 0;

		age_to_count[reset_age] += preg;
		age_to_count[spawn_age] += preg;
	}

	auto debug_print() const -> void {
		std::cerr << "      ";
		for (long n: age_to_count) {
			std::cerr << n << ' ';
		}
		std::cerr << '\n';
	}
};

auto read() -> std::array<long, 9> {
	std::array<long, 9> ret{};

	for (;;) {
		unsigned long n;
		std::cin >> n;
		ret[n]++;
		if (std::cin.get() != ',') {
			break;
		}
	}

	return ret;
}

auto f() -> void {
	Shoal<6, 8> shoal{read()};

	// stdout is for generating the sequence sample for guessing
	// formulas with Fricas.
	//
	//	printf 0 | ./a.out > /tmp/aoc_2021_6_seq.input 2>/dev/null
	std::cout << '[' << shoal.sum() << ", _\n";

	shoal.debug_print();
	std::cerr << "0 " << shoal.sum() << '\n';

	for (long i{1}; i < 300; i++) {
		shoal.advance();

		std::cout << "  " << shoal.sum() << ", _\n";

		shoal.debug_print();
		std::cerr << i << ' ' << shoal.sum() << '\n';
	}

	std::cout << "]\n";
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	f();
}
