// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

auto read_input() -> std::vector<long> {
	std::vector<long> ret;

	for (;;) {
		long t;
		if (!(std::cin >> t)) {
			break;
		}
		ret.push_back(t);
	}

	return ret;
}

template<int window_size>
auto count_increases(const std::vector<long> &v) -> long {
	long count{0};
	for (long i{0}; i < v.size() - window_size; i++) {
		if (v[i] < v[i + window_size]) {
			count++;
		}
	}
	return count;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << count_increases<3>(read_input()) << '\n';
}
