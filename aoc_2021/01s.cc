// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

auto read_input() -> std::vector<long> {
	std::vector<long> ret;

	for (;;) {
		long t;
		if (!(std::cin >> t)) {
			break;
		}
		ret.push_back(t);
	}

	return ret;
}

template<int window_size>
auto sliding_windows(std::vector<long> v) -> std::vector<long> {
	long windows_count{static_cast<long>(v.size() - (window_size - 1))};
	std::vector<long> windows(windows_count);
	for (long i{0}; i < windows_count; i++) {
		windows[i] = 0;
		for (int j{0}; j < window_size; j++) {
			windows[i] += v[i + j];
		}
	}
	return windows;
}

auto count_increases(const std::vector<long> &v) -> long {
	long count{0};
	for (long i{1}; i < v.size(); i++) {
		if (v[i - 1] < v[i]) {
			count++;
		}
	}
	return count;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << count_increases(sliding_windows<3>(read_input())) << '\n';
}
