// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "linked_list.h"
#include "static_caster.h"

namespace {

using namespace linked_list;

using BinaryNumber = std::vector<signed char>;

auto read() -> std::vector<BinaryNumber> {
	std::vector<BinaryNumber> ret;

	for (;;) {
		BinaryNumber num;
		for (;;) {
			long c{std::cin.get()};
			if (c < 0) {
				goto FIN;
			}
			if (c == '\n') {
				break;
			}
			num.push_back(c - '0');
		}
		ret.push_back(num);
	}

FIN:
	for (BinaryNumber &num: ret) {
		std::ranges::reverse(num);
	}

	return ret;
}

auto construct(const BinaryNumber &num) -> unsigned long {
	unsigned long ret{0};
	for (long i{0}; i < num.size(); i++) {
		ret |= static_cast<unsigned long>(num[i]) << i;
	}
	return ret;
}

auto column_sum(const LinkedList<BinaryNumber> &list, long head, long bit) -> long {
	long sum{0};
	for (; !list.is_empty(head); head = list.nodes[head].first) {
		sum += list.nodes[head].second[bit];
	}
	return sum;
}

template<std::predicate<int, int, int> Pred>
auto oxygen_generator_rating(const std::vector<BinaryNumber> &vec) -> long {
	LinkedList<BinaryNumber> list(vec);
	long head{0};

	for (long i = vec[0].size() - 1;; i--) {
		long popcnt{column_sum(list, head, i)};
		long max_popcnt{list.get_length()};

		head = list.erase_if(head, [i, popcnt, max_popcnt](const BinaryNumber &num) -> bool {
			return Pred{}(num[i], popcnt, max_popcnt);
		});

		if (list.is_empty(list.nodes[head].first)) {
			// The list has only got a single element left,
			// return.
			return head;
		}
	}

	// Should be unreachable.
	__builtin_trap();
}

template<bool oxygen>
using Keep = decltype([](int bit, int popcnt, int max_popcnt) -> bool {
	bool ret;
	if (max_popcnt % 2 == 0 && max_popcnt / 2 == popcnt) {
		ret = bit != 0;
	} else {
		ret = bit == (max_popcnt / 2 < popcnt);
	}
	return ret == oxygen;
});

// Returns the index of the remaining BinaryNumber.
template<bool is_oxygen_rating>
auto oxygen_generator_rating(const std::vector<BinaryNumber> &vec) -> long {
	return oxygen_generator_rating<Keep<is_oxygen_rating>>(vec);
}

auto f() -> long {
	using namespace static_caster;
	constexpr Caster<long> l{};

	std::vector<BinaryNumber> data{read()};
	long o{l(construct(data[oxygen_generator_rating<true>(data)]))};
	long c{l(construct(data[oxygen_generator_rating<false>(data)]))};

	return o * c;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << f() << '\n';
}
