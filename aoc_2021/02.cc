// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

template<std::invocable<long> FunForward,
         std::invocable<long> FunUp,
         std::invocable<long> FunDown>
auto lex(FunForward forward, FunUp up, FunDown down) -> void {
	for (;;) {
		long c{std::cin.get()};
		if (c < 0) {
			break;
		}
		long code{c};
		for (; std::cin.get() != ' ';) {}
		long n;
		std::cin >> n;
		std::cin.get();
		if (code == 'f') {
			forward(n);
		} else if (code == 'u') {
			up(n);
		} else {
			down(n);
		}
	}
}

auto g() -> std::pair<long, long> {
	long x = 0, y = 0;

	lex([&x](long n) -> void {
		x += n;
	}, [&y](long n) -> void {
		y -= n;
	}, [&y](long n) -> void {
		y += n;
	});

	return std::pair<long, long>{x, y};
}

auto f() -> long {
	std::pair<long, long> p{g()};
	return p.first * p.second;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << f() << '\n';
}
