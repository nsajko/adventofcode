// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace {

using BinaryNumber = std::vector<signed char>;

auto read() -> std::vector<BinaryNumber> {
	std::vector<BinaryNumber> ret;

	for (;;) {
		BinaryNumber num;
		for (;;) {
			long c{std::cin.get()};
			if (c < 0) {
				goto FIN;
			}
			if (c == '\n') {
				break;
			}
			num.push_back(c - '0');
		}
		ret.push_back(num);
	}

FIN:
	for (BinaryNumber &num: ret) {
		std::ranges::reverse(num);
	}

	return ret;
}

template<std::regular_invocable<long> Fun>
auto binary_number_from_popcnts(const std::vector<long> &popcnts, Fun to_bit) -> BinaryNumber {
	BinaryNumber num;
	for (long popcnt: popcnts) {
		num.push_back(to_bit(popcnt));
	}
	return num;
}

auto construct(const BinaryNumber &num) -> unsigned long {
	unsigned long ret{0};
	for (long i{0}; i < num.size(); i++) {
		ret |= static_cast<unsigned long>(num[i]) << i;
	}
	return ret;
}

auto h(std::vector<BinaryNumber> vec) -> std::vector<long> {
	std::vector<long> popcnts(vec[0].size());
	for (long i{0}; i < vec.size(); i++) {
		const BinaryNumber &num{vec[i]};
		for (long j{0}; j < num.size(); j++) {
			popcnts[j] += num[j];
		}
	}
	return popcnts;
}

auto g(std::vector<BinaryNumber> vec) -> std::pair<long, long> {
	long half_max = vec.size() / 2;
	std::vector<long> popcnts{h(vec)};
	unsigned long dasf{construct(binary_number_from_popcnts(popcnts, [half_max](long n) -> signed char {
		return n < half_max;
	}))};
	int shift = 8*sizeof(dasf) - popcnts.size();
	return std::pair<long, long>{dasf, (~dasf << shift) >> shift};
}

auto f() -> long {
	std::pair<long, long> p{g(read())};
	return p.first * p.second;
}

}  // namespace

auto main() -> int {
	std::ios::sync_with_stdio(false);
	std::cout << f() << '\n';
}
