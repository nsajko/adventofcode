## C++20 solutions for Advent of Code

### Building

Possible build options for GCC (g++) or Clang (clang++): `-std=c++2b -pedantic -pedantic-errors -Wall -Wextra -Wold-style-cast -g -march=native -flto -fno-math-errno -ffp-contract=fast`.

Don't forget to make the headers available: `-I ./include`.

Sanitizers for debugging: `-fsanitize=address,undefined`.
