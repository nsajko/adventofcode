// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include "linked_list.h"

namespace {

using namespace linked_list;

template<typename El>
auto print(const LinkedList<El> &l, long head) -> void {
	std::cout << head << ' ';
	for (; !l.is_empty(head); head = l.nodes[head].first) {
		std::cout << l.nodes[head].second;
	}
	std::cout << '\n';
}

}  // namespace

auto main() -> int {
	LinkedList<int> l(std::vector<int>{0,1,2,5,101,102,-2});
	long head{0};

	print(l, head);

	head = l.erase_if(head, [](int n) -> bool {
		return n % 2 != 0;
	});

	print(l, head);

	head = l.erase_if(head, [](int n) -> bool {
		return n < 0;
	});

	print(l, head);
	head = l.erase_if(head, [](int n) -> bool {
		return n < 10;
	});

	print(l, head);
}
