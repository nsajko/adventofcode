#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

namespace static_caster {

template<typename Out>
using Caster = decltype([](auto x) constexpr {
	return static_cast<Out>(x);
});

}  // namespace static_caster
