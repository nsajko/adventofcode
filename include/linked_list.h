#pragma once

// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace linked_list {

// A singly linked list that does not grow. With a terminating sentinel node.
template<typename El>
class LinkedList {
public:

	using Node = std::pair<long, El>;
	using ValueType = Node;
	using Reference = Node &;
	using ConstReference = const Node &;
	using SizeType = long;

	// Last node is a sentinel node.
	std::vector<Node> nodes;

private:

	long length;

public:

	auto connect_in_order() -> void {
		for (long i{0}; i < nodes.size() - 1; i++) {
			nodes[i].first = i + 1;
		}
		length = nodes.size() - 1;
	}

	LinkedList(): nodes{std::vector<Node>(1, Node{-1, El{}})} {}

	LinkedList(const LinkedList<El> &) = default;

	LinkedList(LinkedList<El> &&) = default;

	LinkedList(long count): nodes{std::vector<Node>(count + 1)} {
		connect_in_order();
	}

	LinkedList(long count, const El &el): nodes{std::vector<Node>(count + 1, Node{0, el})} {
		connect_in_order();
	}

	LinkedList(const std::vector<El> &v): nodes{std::vector<Node>(v.size() + 1)} {
		connect_in_order();
		for (long i{0}; i < v.size(); i++) {
			nodes[i].second = v[i];
		}
	}

	auto is_empty(long head) const -> bool {
		return head == nodes.size() - 1;
	}

	// Hack!
	auto get_length() const -> long {
		return length;
	}

	auto erase(long prev) -> void {
		nodes[prev].first = nodes[nodes[prev].first].first;
		length--;
	}

	// Returns the new head.
	template<std::predicate<const El &> Pred>
	auto erase_if(long head, Pred pred) -> long {
		// Delete matching elements in prefix while updating head.
		for (; !is_empty(head) && pred(nodes[head].second);) {
			head = nodes[head].first;
			length--;
		}

		nodes.back().first = nodes.size() - 1;

		// Delete the rest of matching elements.
		for (long t{head};;) {
			long next{nodes[t].first};

			if (is_empty(next)) {
				break;
			}

			if (pred(nodes[next].second)) {
				erase(t);
			} else {
				t = next;
			}
		}

		return head;
	}
};

}  // namespace linked_list
